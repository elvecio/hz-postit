import React, { useState, useContext, createContext, useEffect } from "react";
import { SafeAreaView, ScrollView, View, Text, TextInput, Button, Pressable, Alert, StyleSheet, Modal, Keyboard} from "react-native";

import DocumentPicker from "react-native-document-picker";
import RNFS from "react-native-fs";
import { Buffer } from 'buffer';
import ReceiveSharingIntent from 'react-native-receive-sharing-intent';
import RNSecureStorage, {ACCESSIBLE} from 'rn-secure-storage';

const AppContext = React.createContext(null);

const App = () => {
  const [file, setFile] = useState(null);
  const [fileName, setFileName] = useState('');
  const [body, setBody] = useState('');
  const [postStatus, setPostStatus] = useState('');
  const [intentVal, setIntentVal] = useState('');
  const [hub, setHub] = useState('');
  const [channel, setChannel] = useState('');
  const [channelHash, setChannelHash] = useState('');
  const [password, setPassword] = useState('');
  const [secret, setSecret] = useState('');
  const [audience, setAudience] = useState('');

  useEffect(() => {
    RNSecureStorage.multiGet(['hub', 'channel', 'channelHash', 'password']).then((res) => {
      console.log('start', res);
      if (res.hub && res.channel && res.channelHash && res.password) {
        setHub(res.hub);
        setChannel(res.channel);
        setChannelHash(res.channelHash);

        let secret = Buffer.from(res.channel + ':' + res.password, 'utf-8').toString('base64');
        setSecret(secret);
      }
    }).catch((err) => {
      console.log(err);
    });
  }, [secret]);

  useEffect(() => {
    if (hub && secret) {
      fetchGroups({'hub': hub, 'secret': secret});
    }
  }, []);

  ReceiveSharingIntent.getReceivedFiles(async files => {
    if (files[0].fileSize !== null) {
      setFileName(files[0].fileName);
      let file = {'name': files[0].fileName, 'size': files[0].fileSize, 'type': files[0].mimeType, 'uri': 'file://' + files[0].filePath}
      setFile(file);
    }
    else {
      setBody(files[0].weblink || files[0].text);
    }
    // setIntentVal(JSON.stringify(files[0], null, 2));
  }, (error) => {
    //console.log('ReceiveSharingIntent.getReceivedFiles:', error);
  }, 'hzpostit'); // share url protocol (must be unique to your app, suggest using your apple bundle id)

  return (
    <SafeAreaView>
      <ScrollView keyboardShouldPersistTaps={'handled'}>
        <AppContext.Provider value={{
          file, setFile,
          fileName, setFileName,
          body, setBody,
          postStatus, setPostStatus,
          hub, setHub,
          channel, setChannel,
          channelHash, setChannelHash,
          password, setPassword,
          secret, setSecret,
          audience, setAudience,
          intentVal, setIntentVal
        }}>
          <View style={{margin: 10}}>
          {!secret && <SignIn/>}
          {secret && <Text style={styles.headerLC}>{channel}@{hub}</Text>}
          {secret && <Post/>}
          {secret && <FileSelect/>}
          {secret && <Audience/>}
          {secret && <Text selectable={true}>{intentVal}</Text>}
          </View>
        </AppContext.Provider>
      </ScrollView>
    </SafeAreaView>
  );
}

const FileSelect = () => {
  const appContext = useContext(AppContext);

  const pickFile = (async () => {
    Keyboard.dismiss();

    try {
      let res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });
      appContext.setFile(res[0]);
      appContext.setFileName(res[0].name);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        console.log("User cancelled the picker, exit any dialogs or menus and move on");
      } else {
        throw err;
      }
    }
  });

  return (
    <View>
      <Button title="Choose file" onPress={pickFile} />
      <Text>{appContext.fileName}</Text>
    </View>

  );
};

const Post = () => {
  const appContext = useContext(AppContext);
  const post = async () => {
    Keyboard.dismiss();

    try {
        appContext.setPostStatus('Sending...');
        let credentials = {
          'hub': appContext.hub,
          'secret': appContext.secret,
          'audience': appContext.audience === 'self' ? appContext.channelHash : appContext.audience,
          'hash': appContext.channelHash
        };

        if (!appContext.file) {
          await postStatus(appContext.body, credentials);
        }
        else {
          await uploadFileInChunks(appContext.file, appContext.body, credentials);
        }
        appContext.setFile(null);
        appContext.setFileName('');
        appContext.setBody('');
        appContext.setPostStatus('Completed :)');
        setTimeout(() => { appContext.setPostStatus(''); }, 2000);

    } catch (err) {
      throw err;
    }
  };

  return (
    <View>
      <View>
        <TextInput
          multiline={true}
          numberOfLines={8}
          placeholder="Write something"
          textAlignVertical="top"
          value={appContext.body}
          onChangeText={(text) => appContext.setBody(text)}
        />
        <Button title="Post" onPress={post} />
        <Text>{appContext.postStatus}</Text>
      </View>
    </View>
  );
};

const fetchGroups = async (credentials) => {
  try {
    console.log('fetchig groups')
    let url = 'https://' + credentials.hub + '/api/z/1.0/group';
    let res = await fetch(url, {
      method: 'POST',
      headers: {
        'Authorization': 'Basic ' + credentials.secret,
      }
    });

    let status = await res.status;
    if (status !== 200) {
      return;
    }

    let data = await res.json();
    let groups = [];

    data.map((group) => {
      if (!group.deleted) {
        groups.push({'name': group.gname, 'hash': group.hash })
      }
    });

    RNSecureStorage.setItem('groups', JSON.stringify(groups), {accessible: ACCESSIBLE.WHEN_UNLOCKED});

  } catch (error) {
    console.log('fetchGroups error', error);
  }
}

const postStatus = async (body, credentials) => {
  try {
    let url = 'https://' + credentials.hub + '/api/z/1.0/item/update';

    let formData = new FormData();
    formData.append('body', body);

    if (credentials.audience === credentials.hash) {
      formData.append('contact_allow', credentials.audience);
    }
    else {
      formData.append('group_allow', credentials.audience);
    }

    await fetch(url, {
      method: 'POST',
      headers: {
        'Authorization': 'Basic ' + credentials.secret,
        'Content-Type': 'multipart/form-data',
      },
      body: formData
    });
    console.log('postStatus complete');

  } catch (error) {
    console.log('postStatus error', error);
  }
}

const uploadFileInChunks = async (currentFile, body, credentials) => {
  try {
    let uploadUrl = 'https://' + credentials.hub + '/api/z/1.0/item/update';
    let chunkSize = 1024 * 1024 * 4;
    let fileSize = currentFile.size;
    let chunks = Math.ceil(fileSize/chunkSize);
    let chunksProcessed = 1;
    let offset = 0;
    let limit = (((offset + chunkSize) < fileSize) ? (offset + chunkSize) : fileSize);

    while (chunksProcessed <= chunks) {
      //here we are reading only a small chunk of the file.
      const chunk = await RNFS.read(currentFile.uri, chunkSize, offset, 'base64');

      let path = RNFS.DocumentDirectoryPath + '/chunk';

      // write the chunk on disk otherwise we could not convince axios to properly send it
      await RNFS.writeFile(path, chunk, 'base64');

      // cunstruct an object linking to the chunk
      let file = {'name': currentFile.name, 'type': currentFile.type, "uri": 'file://' + path}

      let formData = new FormData();
      formData.append('media', file);
      formData.append('body', body);
      if (credentials.audience === credentials.hash) {
        formData.append('contact_allow', credentials.audience);
      }
      else {
        formData.append('group_allow', credentials.audience);
      }

      await fetch(uploadUrl, {
        method: 'POST',
        headers: {
          'Authorization': 'Basic ' + credentials.secret,
          'Content-Type': 'multipart/form-data',
          'Content-Range': 'bytes ' + offset + '-' + (limit - 1) + '/' + fileSize
        },
        body: formData
      });

      chunksProcessed++;
      offset += chunkSize;
      limit = (((offset + chunkSize) < fileSize) ? (offset + chunkSize) : fileSize);
    }
    console.log('Upload complete');

  } catch (error) {
    console.error('Error during chunk upload:', error);
  }
};

const SignIn = () => {
  const appContext = useContext(AppContext);

  const signIn = (async () => {
    Keyboard.dismiss();
    try {
      let url = 'https://' + appContext.hub + '/api/z/1.0/verify';
      let secret = Buffer.from(appContext.channel + ':' + appContext.password, 'utf-8').toString('base64');
      let res = await fetch(url, {
        method: 'POST',
        headers: {
          'Authorization': 'Basic ' + secret
        },
      });

      let status = await res.status;

      if (status !== 200) {
        Alert.alert('Error', 'Could not connect.')
        return false
      }

      let json = await res.json();

      if (!json) {
        Alert.alert('Error', 'Could not fetch data.')
        return false
      }

      await fetchGroups({'hub': appContext.hub, 'secret': secret});

      try {
        RNSecureStorage.multiSet(
          {
            'hub': appContext.hub,
            'channel': json.channel_address,
            'channelHash': json.channel_hash,
            'password': appContext.password
          },
          {
            accessible: ACCESSIBLE.WHEN_UNLOCKED
          }
        ).then((res) => {
          console.log(res);
          appContext.setSecret(secret);
        }).catch((err) => {
          console.log(err);
        });
      } catch (err) {
        console.log('signIn error storing', error);
      }
    } catch (error) {
      console.log('signIn error', error);
      return;
    }
  });

  return (
    <View>
        <TextInput
          placeholder="yourhub.tld"
          value={appContext.hub}
          onChangeText={(text) => appContext.setHub(text)}
        />
        <TextInput
          placeholder="Channel name"
          value={appContext.channel}
          onChangeText={(text) => appContext.setChannel(text)}
        />
        <TextInput
          placeholder="Password"
          secureTextEntry={true}
          value={appContext.password}
          onChangeText={(text) => appContext.setPassword(text)}
        />
        <Button title="Sign in" style={styles.button} onPress={signIn} />
    </View>

  );
};

const Audience = () => {
  const appContext = useContext(AppContext);
  const [modalVisible, setModalVisible] = useState(false);
  const [storedGroups, setStoredGroups] = useState(null);
  const [selectedAudience, setSelectedAudience] = useState('Public');
  const [storedGroupsArray, setStoredGroupsArray] = useState([{'name': 'Public', 'hash': ''}, {'name': 'Only me', 'hash': 'self'}]);

  useEffect(() => {
    RNSecureStorage.getItem('groups').then((res) => {
        setStoredGroupsArray(storedGroupsArray.concat(JSON.parse(res)));
    }).catch((err) => {
        console.log(err);
    });
  }, []);

  return (
    <View>
      <Modal
        animationType="none"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.header}>Select your audience</Text>
            {storedGroupsArray.map((g) => {
              return <Pressable key={g.hash} style={{padding: 10}} onPress={() => {appContext.setAudience(g.hash); setSelectedAudience(g.name); setModalVisible(!modalVisible);}}><Text>{g.name}</Text></Pressable>;
            })}
          </View>
        </View>
      </Modal>
      <Button title={selectedAudience} color="orchid" onPress={() => setModalVisible(true)} />
    </View>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    margin: 10,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 20,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOpacity: 0.25,
    shadowRadius: 7,
    elevation: 5,
  },
  header: {
    marginBottom: 10,
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderColor: '#aaa',
    textTransform: 'uppercase',
  },
  headerLC: {
    marginBottom: 10,
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderColor: '#aaa',
    textTransform: 'lowercase',
  }

});

export default App;
